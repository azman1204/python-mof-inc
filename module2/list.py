# list
# index list bermula dari 0
name = ['abu', 'ali', 'ahmad']

print(name[1]) # print value dari list di position / index 1
print(name[-1]) # ambil value last sekali
print(name[-2]) # ambil 2nd last value
print(name[0:2]) # ['abu', 'ali'] - start index 0, sehinnga index 2, but exclude 2
print(name[0:]) # ['abu', 'ali', 'ahmad'] - first hingga last
print(name[:2]) # ['abu', 'ali'] - first sehinnga index tetapi exclude last index

# tule sama sepeti list
# apa beza tuple dgn list. tuple constant, value x boleh ubah
# utk performance
ages = (10, 30, 20, 50)
print (ages[0]) 

# dictionary {key : value}
# sama sebahagian konsep spt object, tetapi tiada custom function
# value boleh ubah, key boleh bertambah
person = {"name": 'azman', 'age': 45, 'isMarried': True}
print(person['name'])
person['address'] = 'Bangi' # insert a new key:value
print(person['address'])

# example dictionary dalam list
list_person = [{'name':'abu'}, {'name':'ali'}]
print(list_person[1]['name'])


