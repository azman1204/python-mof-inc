# string , numeric, boolean - default in python
# custom type - class / object
# Python case-sensitive pro. lang

# this is comment. short cut ctl + /

# string variable
msg = "Hello World" # infer as string
# in java String msg = "Hello World"
print(msg) # ctl + alt + n = run

# numeric variable
age = 45 # infer as numeric (int)
print(type(age))

price = 45.5 # infer as numeric (int)
print(type(price))

isMarried = True # True or False
print(type(isMarried))

# tuple
names = ('azman', 'daniel', 'abu')
print(type(names))

# list
ages = [1,2,3,4,5] # list of number
print(type(ages))

lnames = ['abu', 'ali', 'ahmad'] # list of string
lboolean = [True, False, True, True]

# dictionary
person = {'name': 'abu', 'address':'Bangi'}
print(type(person))




