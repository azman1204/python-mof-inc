from flask import Flask, render_template, request, redirect
import db

app = Flask(__name__)

@app.route('/')
def home():
    return 'Hello'

# list all employee
@app.route('/employee/list')
def listing():
    con, cur = db.get_cursor()
    sql = "SELECT * FROM employee"
    rs = cur.execute(sql)
    result = rs.fetchall()
    print(result)
    return render_template('employee/list.html', rows=result)


@app.route('/employee/create')
def create():
    return render_template('employee/form.html')


@app.route('/employee/save', methods=['POST'])
def save():
    name  = request.form['name']
    email = request.form['email']
    post  = request.form['post']

    con, cur = db.get_cursor()
    sql = f"INSERT INTO employee (name, email, post) VALUES ('{name}', '{email}', '{post}')"
    cur.execute(sql)
    con.commit()
    return redirect('/employee/list')


@app.route('/employee/edit/<emp_id>')
def edit(emp_id):
    con, cur = db.get_cursor()
    sql = f"SELECT * FROM employee WHERE id = {emp_id}"
    rs = cur.execute(sql)
    employee = rs.fetchone() # return a tuple
    # print(employee)
    return render_template('employee/edit.html', employee = employee)


@app.route('/employee/update', methods=['POST'])
def update():
    name  = request.form['name']
    email = request.form['email']
    post  = request.form['post']
    id    = request.form['id']

    con, cur = db.get_cursor()
    sql = f"UPDATE employee SET name='{name}', email='{email}', post='{post}' WHERE id = {id}"
    rs = cur.execute(sql)
    con.commit()
    return redirect('/employee/list')


@app.route('/employee/delete/<emp_id>')
def delete(emp_id):
    con, cur = db.get_cursor()
    sql = f"DELETE FROM employee WHERE id = {emp_id}"
    rs = cur.execute(sql)
    con.commit()
    return redirect('/employee/list')
    

if __name__ == '__main__':
    app.run(debug=True)