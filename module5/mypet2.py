from animal import Dog, Cat, hello, name

dog1 = Dog('Johnny')
print('Name = ' + dog1.name)

cat1 = Cat('Meow')
print('Cat name = ', cat1.name)