# create a class Dog
# have 2 properties: 1. name, 2 breed
# have a constructor which have 2 parameter: 
# 1. name, 2. breed

class Dog:
    name = ''
    breed = ''

    def __init__(self, name, breed):
        # initialize property
        self.name = name
        self.breed = breed

    def print_info(self):
        print(f'name = {self.name}, breed = {self.breed}')


dog1 = Dog('Johnny', 'Persian')
dog1.print_info()

dog2 = Dog('Milo', 'German')
dog2.print_info()