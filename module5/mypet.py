# animal -> module
import animal

dog1 = animal.Dog('Johnny')
print('Name = ' + dog1.name)
# print(f'Name = {dog1.name}')

cat1 = animal.Cat('Meow')
print('Cat Name = ', cat1.name)
