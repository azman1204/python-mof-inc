# inheritance

class Vehicle:
    name = ''
    brand = ''
    num_of_tyre = 0

    def print_info(self):
        print(f'name = {self.name} brand = {self.brand} num tyre = {self.num_of_tyre}')


class Car(Vehicle):
    type = 'sedan'

    # override function print_info
    def print_info(self):
        print(f'''
        name     = {self.name} 
        brand    = {self.brand} 
        num tyre = {self.num_of_tyre}
        type     = {self.type}''')

class Lorry(Vehicle):
    horse_power = 0

    def __init__(self, name2, brand2, num_tyre2, horse_power2):
        self.name = name2
        self.brand = brand2
        self.num_of_tyre = num_tyre2
        self.horse_power = horse_power2

class Bicycle(Vehicle):
    pass # tiada code


car = Car()
car.name = 'Proton Saga'
car.brand = 'Proton'
car.num_of_tyre = 4
car.print_info()

lorry = Lorry('Tanker', 'Hino', 16, 30)
lorry.print_info()
# lorry = Lorry()
# lorry.name = 'Tanker'
# lorry.brand = 'Hino'
# lorry.num_of_tyre = 16
# lorry.horse_power = 30
# lorry.print_info()