class Cat:
    # properties
    color = 'Grey'
    breed = 'Persian'
    name  = 'Comel'

    # method
    def eat(self):
        print(f'{self.name} is eating')

# create a cat object
cat1 = Cat()
cat1.eat() # Comel is eating
print(f'color: {cat1.color} breed: {cat1.breed}')

cat2 = Cat()
cat2.name = 'Oyange'
cat2.eat() # Oyange is eating