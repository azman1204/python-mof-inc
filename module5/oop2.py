# class is a collection of function / cohesive methods
class Vehicle:
    # properties
    made = 'Japan'
    brand = 'Toyota'

    # methods

    # constructor. auto run.
    # initialize property
    def __init__(self):
        # print('i''m in constructor..')
        print("i'm in constructor..")

    def show_info(self):
        print(f'made = {self.made} brand = {self.brand}')
    
    def start_engine(self):
        print('vrom..vrom..')

    def vehicle_type(self, num_tyre = 4):
        if num_tyre == 4:
            print('4 tyres')
        else:
            print('2 tyres')


# create obj dari class - instance / instantiate
car = Vehicle()
car.show_info() # made = Japan brand = toyota

lorry = Vehicle()
lorry.brand = 'Mitsubishi' # set value in a property
print(lorry.brand) # get a property value. Mitsubishi
lorry.show_info() # made = Japan brand = Mitsubishi
lorry.vehicle_type(2)