from flask import Flask, render_template, request

app = Flask(__name__)

# default URL http://localhost:5000/
@app.route("/")
def hello_world():
    #return "<p>Hello, World!</p>"
    return render_template('hello_world.html', nama='Azman')


# http://localhost:5000/hello
@app.route('/hello')
def hello():
    return "<h2>Hello by azman...abcdef</h2>"  


@app.route('/calculator')
def calc():
    return render_template('calculator.html')  


@app.route('/investment-calculator', methods=['POST'])
def calculator():
    invest_amount = int(request.form['amount'])
    num_years     = int(request.form['years'])
    profit_rate   = int(request.form['rate'])
    html = ''
    for year in range(1,num_years + 1):
        profit = round(invest_amount * (profit_rate / 100))
        invest_amount = invest_amount + profit
        html = html + f'{year}  {profit}  {invest_amount} <br>'

    return html

# auto refresh / auto restart
# turn off masa production
if __name__ == '__main__':
    app.run(debug=True)

