# create a function, with 3 parameter
# 1. amount investment
# 2. profit rate %
# 3. num of years to invest
# 4. print year 1 ... 10, profit, amount investment + profit


def investment_calc(invest_amount, profit_rate, num_years):
    for year in range(1,num_years + 1):
        # round() built-in function in python
        profit = round(invest_amount * (profit_rate / 100))
        invest_amount = invest_amount + profit
        print(f'{year}  {profit}  {invest_amount}')
    print('***' * 10)

# investment_calc(100000, 5, 10)
investment_calc(500000, 5, 15)