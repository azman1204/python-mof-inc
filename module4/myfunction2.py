# function yg return value

def campur(num1, num2):
    # local variable scope
    total = num1 + num2
    return total


# globl scope
total = campur(5, 4)
print(f'total = {total}')