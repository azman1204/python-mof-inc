# function ada 2 jenis:
# 1. return
# 2. not return value

def hello():
    print('Hello')

# call function
hello()
hello()
hello()

# function with parameter / arguments
# name = parameter
def hello2(name):
    print(f'Hello {name}')

hello2('azman')
hello2('John Doe') # John Doe = argument


def hello3(name, address):
    print(f'Hello {name} you living in {address}')

hello3('Jane Doe', 'London')
# hello3('London 2')#  ini error

# function with optional parameter
# dlm kes ini address menjadi optional, because it have default value
# once satu paramete is optional, next parameter must also optional
def hello4(name, address='London'):
    print(f'name = {name} address = {address}')

print('--' * 30)
hello4('Azman')
hello4('Ahmad', 'Ipoh')
hello4(name='Ali', address='Perlis')