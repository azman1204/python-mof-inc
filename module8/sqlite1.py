import sqlite3

def print_fix(dat, flength=20):
    length = len(dat)
    balance = flength - length
    return dat + " " * balance

# con = sqlite3.connect('training.db') # default lokasi db ialah di mana directori python run
con = sqlite3.connect('C:\\Users\\azman\\Desktop\\python training\\training.db')
cur = con.cursor()
result = cur.execute("SELECT * FROM employee")
# row = result.fetchone()
# print(row)

rows = result.fetchall()
# print(rows)
print(f'{print_fix("ID", 5)} {print_fix("Name", 15)} {print_fix("Email", 20)} {print_fix("Post", 15)}')

for emp in rows:
    print(f"{print_fix(str(emp[0]), 5)} {print_fix(str(emp[1]), 15)}", end=' ')
    print(f"{print_fix(str(emp[2]), 20)} {print_fix(str(emp[3]), 15)}")


