# ask input for cmd
# cmd : dir -> list dir
# cmd : mkdir -> create dir
import os

path = "c:\\sample"
while True:
    str = input(">").split() # convert string input ke list
    cmd = str[0]

    if cmd == 'dir':
        files = os.listdir(path)
        for fname in files:
            print(fname)
    elif cmd == 'mkdir':
        dirname = str[1] # nama folder
        os.mkdir(path + "\\" + dirname) # c:\sample\testing
