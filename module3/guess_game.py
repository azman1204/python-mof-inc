# gen satu random no. random.random() between 1 dan 100
# teka no tersebut
# jika betul, print "you win"
# jisa salah, print "teka lagi, higher or lower"
import random

no = round(random.random() * 100) # gen no between 0 .. 1
# print(f'no = {no}')

while True:
    input_no = int(input("Masukkan satu no : "))

    if input_no == no:
        # betul
        print('You win')
        break # exit loop
    else:
        # salah
        if input_no > no:
            print('salah, teka lebih rendah')
        else:
            print('salah, teka lebih tinggi')

