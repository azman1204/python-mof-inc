isMarried = False 

if isMarried:
    print('Sudah berkahwin')

if isMarried: 
    pass # nothing to run here. cannot empty line here

if isMarried:
    print('Dah kahwin')
else:
    print('Belum kahwin')

animal = input('enter animal name : ') # asking a value in terminal

if animal == 'Bird':
    print('a bird')
elif animal == 'cat':
    print('a cat')
elif animal == 'dog':
    print('a dog')
else:
    print('not sure')

# true and true = true, true and false = false
# true or true = true, true or false = true, false or false = false
if animal == 'Bird' and animal == 'cat':
    print('bird and cat') # this logic never run

if animal == 'Bird' or animal == 'cat':
    print('bird or cat')

# group logic
if (animal == 'Bird' or animal == 'cat') and animal == 'dog':
    print('bird or cat and dog') # logic will never run

