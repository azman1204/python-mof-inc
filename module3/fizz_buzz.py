# printkan perkataan fizz jika satu nombor boleh dibahagi dengan 3
# pintkan perkataan buzz jika satu nombor boleh dibahagi dengan 5
# printkan FizzBuzz jika nombor boleh dibahagi dengan 3 dan 5
# jika tidak boleh dibahagi dengan 3 atau 5, printkan no tersebut

# hint : 10 % 3 = 1

no = 15

# if no % 3 == 0 or no % 5 == 0:
#     if no % 3 == 0:
#         print('Fizz')

#     if no % 5 == 0:
#         print('Buzz')
# else:
#     print(no)


if no % 3 != 0 and no % 5 != 0:
    print(no)

if no % 3 == 0:
    print('Fizz')

if no % 5 == 0:
    print('Buzz')