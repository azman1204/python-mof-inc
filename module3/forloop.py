# dlm python ad 2 jenis loop
# 1. for loop, 2. while loop

# contoh for loop. loop khas utk list / collection
for bil in [1,2,3,4,5]:
    print(f'bil = {bil}') # f = format. interpolation
    print('bil2 = ' + str(bil))

for name in ['abu', 'ali', 'ahmad']:
    print('nama = ' + name) # + = concate 

print('-' * 15) # ---------------
# while loop
bil = 0
while bil <= 10:
    print(f'bil = {bil}')
    bil = bil + 1

print('--------------------------')
for num in range(0, 11, 2):
    print(f'num = {num}')