-- CRUD = create retrieve update delete

insert into employee(name, email, post)
values('John Doe', 'john@gmail.com', 'Programmer');

insert into employee(name, email, post)
values('John Smith', 'smith@gmail.com', 'Data Scientist');

insert into employee(name, email, post)
values('Jane Doe', 'jane@gmail.com', 'Software Engineer');

select * from employee;